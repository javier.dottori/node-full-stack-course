const rlp = require('readline');
/*
const rl = rlp.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: true
});

function ask() {
  return new Promise((resolve) => {
    rl.question('', (r) => { resolve(r) })
  })
}

async function run() { 
  console.log('Ask: ');
  let response = await ask();
  console.log(response);
  
  console.log('Ask: ');
  response = await ask();
  
  console.log('Ask: ');
  response = await ask();
  console.log(response);

  console.log(response);
  return null;
}

run().then(function () {
  
});
*/

const rl = rlp.createInterface({
  input: process.stdin,
  output: process.stdout
})

function askName() {
  return new Promise((resolve) => {
    rl.question('What is your name? ', (name) => { resolve(name) })
  })
}

function askAge(name) {
  return new Promise((resolve) => {
    rl.question('What is your age? ', (age) => { resolve([name, age]) })
  })
}

function outputEverything([name, age]) {
  console.log(`Your name is ${name} and your age is ${age}`)
}

askName().then(askAge).then(outputEverything)
/*

const readline = require('readline-promise');
const fs = require('fs');
 
console.log(readline);

const stdinout = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })

 

const rlp = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
  });

let bar = null;

rlp.questionAsync('Foo?').then(answer => {
bar = answer;
});

*/
const readline = require('readline');

function ask(text) {
    let rl = readline.createInterface(process.stdin, process.stdout);
    rl.setPrompt(text+'> ');
    rl.prompt();
    let p = new Promise(function(res, rej){
        rl.on('line', function(line) {
            rl.close();
            res(line);
        })    
    })
    return p
}

async function run() {
    x = await ask("question");
    console.log("Rta: " + x);
    x = await ask("question2");
    console.log("Rta2: " + x);

}

run();